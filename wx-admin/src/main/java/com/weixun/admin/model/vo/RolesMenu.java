package com.weixun.admin.model.vo;

/**
 * Created by myd on 16/4/5.
 */
public class RolesMenu {
    private String rolesmenu_pk;
    private String fk_roles_pk;
    private String fk_menu_pk;
    private String checked;

    public String getRolesmenu_pk() {
        return rolesmenu_pk;
    }

    public void setRolesmenu_pk(String rolesmenu_pk) {
        this.rolesmenu_pk = rolesmenu_pk;
    }

    public String getFk_roles_pk() {
        return fk_roles_pk;
    }

    public void setFk_roles_pk(String fk_roles_pk) {
        this.fk_roles_pk = fk_roles_pk;
    }

    public String getFk_menu_pk() {
        return fk_menu_pk;
    }

    public void setFk_menu_pk(String fk_menu_pk) {
        this.fk_menu_pk = fk_menu_pk;
    }

    public String isChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
